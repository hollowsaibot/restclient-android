package velasco.carlos.restclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRPostman;

public class EditorCollectionActivity extends AppCompatActivity {

    private EditorCollectionActivity self = this;

    private SRCollection model;

    @InjectView(R.id.etCollectionName)
    EditText etCollectionName;

    @InjectView(R.id.etCollectionDescription)
    EditText etCollectionDescription;

    @InjectView(R.id.chkCollectionPublic)
    CheckBox chkPublic;

    @InjectView(R.id.btnSaveCollection)
    FloatingActionButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_collection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(getTitle());

        ButterKnife.inject(this);

        model = getModel(self);

        loadViewFromModel(model);

        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateModelFromView(model);
                SoftKeyboardHelper.hideKeyboard(self);

                Snackbar.make(view, getString(R.string.editor_collection_saved_message), Snackbar.LENGTH_LONG)
                        .setAction(R.string.snackbar_action_done, clickListener).show();
            }
        });
    }

    private void loadViewFromModel(SRCollection model) {
        etCollectionName.setText(model.getName());
        etCollectionDescription.setText(model.getDescription());
        Boolean isPublic = model.getPublicCollecion();
        if(isPublic == null) isPublic = false;
        chkPublic.setChecked(isPublic);
    }

    private void updateModelFromView(SRCollection model) {
        model.setName(etCollectionName.getText().toString());
        model.setDescription(etCollectionDescription.getText().toString());
        model.setPublicCollecion(chkPublic.isChecked());

        // set status
        Intent resultIntent = new Intent();
        resultIntent.putExtra(SRPostman.class.getName(), model);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    private SRCollection getModel(Activity activity) {
        Intent intent = activity.getIntent();
        SRCollection tmpCollection = (SRCollection) intent.getSerializableExtra(SRCollection.class.getName());
        if(tmpCollection == null) {
            tmpCollection = new SRCollection();
        }
        // return model
        return tmpCollection;
    }
}
