package velasco.carlos.restclient.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import velasco.carlos.restclient.R;

public class NavigationStringAdapter extends ArrayAdapter<String> {

    Context context; 
    int layoutResourceId;    
    String data[] = null;
    
    public NavigationStringAdapter(Context context, int layoutResourceId, String[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        StringHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new StringHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            
            row.setTag(holder);
        }
        else
        {
            holder = (StringHolder)row.getTag();
        }
        
        String option = data[position];
        holder.txtTitle.setText(option);
        
        return row;
    }
    
    static class StringHolder
    {
        TextView txtTitle;
    }
}