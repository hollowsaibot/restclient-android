package velasco.carlos.restclient.adapters;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.R;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRRequestContainer;

public class RequestContainerListAdapter extends ArrayAdapter<RequestContainerListAdapter.SRRequestContainerItem> {

    private List<SRRequestContainerItem> data;
    private LayoutInflater inflater;
    private Context context;

    public RequestContainerListAdapter(Context ctx, ArrayList<SRRequestContainerItem> items) {

        super(ctx, -1, items);

        inflater = LayoutInflater.from(ctx);
        this.context = ctx;
        this.data = items;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public SRRequestContainerItem getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        /* create a new view of my layout and inflate it in the row */
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            Holder holder = new Holder(convertView);
            convertView.setTag(holder);
        }

        /* Extract the city's object to show */
        SRRequestContainerItem item = getItem(position);
        Holder holder = (Holder) convertView.getTag();

        /* Take the TextView from layout and set the city's name */
        holder.text.setText(item.text);

        return convertView;
    }

    public static class SRRequestContainerItem {

        private String text;
        private String collectionId;
        private String folderId;
        private Boolean isCollection;

        /* Getters & Setters */
        public String getCollectionId() {
            return collectionId;
        }

        public void setCollectionId(String collectionId) {
            this.collectionId = collectionId;
        }

        public String getFolderId() {
            return folderId;
        }

        public void setFolderId(String folderId) {
            this.folderId = folderId;
        }

        public Boolean getIsCollection() {
            return isCollection;
        }

        public void setIsCollection(Boolean isCollection) {
            this.isCollection = isCollection;
        }

        /* Aux methods */
        public static ArrayList<SRRequestContainerItem> convertCollectionsToItems(List<SRCollection> collections){
            ArrayList<SRRequestContainerItem> items = new ArrayList<>();
            for(SRCollection collection : collections){
                SRRequestContainerItem item = convertRequestContainerToItem(collection);
                items.add(item);
                for(SRFolder folder : collection.getFolders()){
                    SRRequestContainerItem itemFolder = convertRequestContainerToItem(collection, folder);
                    items.add(itemFolder);
                }
            }
            return items;
        }

        public static SRRequestContainerItem convertRequestContainerToItem(SRCollection collection){
            SRRequestContainerItem item = new SRRequestContainerItem();
            item.text = String.format("%s", collection.getName());
            item.collectionId = collection.getId();
            item.folderId = null;
            item.isCollection = true;
            return item;
        }

        public static SRRequestContainerItem convertRequestContainerToItem(SRCollection collection, SRFolder folder){
            SRRequestContainerItem item = new SRRequestContainerItem();
            item.text = String.format("%s/%s", collection.getName(), folder.getName());
            item.collectionId = collection.getId();
            item.folderId = folder.getId();
            item.isCollection = false;
            return item;
        }
    }

    public class Holder {

        @InjectView(android.R.id.text1)
        TextView text;

        public Holder(View view) {

            ButterKnife.inject(this, view);
        }
    }
}