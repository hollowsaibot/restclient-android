package velasco.carlos.restclient.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import velasco.carlos.restclient.R;
import velasco.carlos.restclient.models.SRCollection;

public class NavigationSRCollectionAdapter extends ArrayAdapter<SRCollection> {

    Context context;
    int layoutResourceId;
    ArrayList<SRCollection> data = null;

    public NavigationSRCollectionAdapter(Context context, int layoutResourceId, ArrayList<SRCollection> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        StringHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new StringHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);

            row.setTag(holder);
        } else {
            holder = (StringHolder) row.getTag();
        }

        SRCollection option = data.get(position);
        holder.txtTitle.setText(option.getName());

        return row;
    }

    static class StringHolder {
        TextView txtTitle;
    }
}