package velasco.carlos.restclient.adapters;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.R;

public class ParamListAdapter extends ArrayAdapter<Map.Entry<String, String>> {

    private final Map<String, String> data;
    private LayoutInflater inflater;
    private Context context;
    private Fragment fragment;
    private ArrayAdapter self;

    public View.OnClickListener deleteCallback;

    public ParamListAdapter(Context ctx, Fragment fragment, final Map<String, String> objects, View.OnClickListener deleteCallback) {

        super(ctx, -1, new ArrayList<Map.Entry<String, String>>() {{
            addAll(objects.entrySet());
        }});
        inflater = LayoutInflater.from(ctx);
        this.fragment = fragment;
        this.context = ctx;
        this.data = objects;
        this.deleteCallback = deleteCallback;
        self = this;
    }

    public void update() {
        super.clear();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            // get item
            super.add(entry);
        }
        super.notifyDataSetChanged();
    }

    @Override
    public void add(Map.Entry<String, String> object) {
        super.add(object);
        data.put(object.getKey(), object.getValue());
        super.notifyDataSetChanged();
    }

    @Override
    public void remove(Map.Entry<String, String> object) {
        super.remove(object);

        data.remove(object.getKey());
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        /* create a new view of my layout and inflate it in the row */
        if (convertView == null) {
            convertView = (RelativeLayout) inflater.inflate(R.layout.layout_item_param, null);
            Holder holder = new Holder(convertView);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteCallback != null) {
                        deleteCallback.onClick(v);
                    }
                    /* Remove the object to show */
                    remove(getItem(position));
                }
            });
            convertView.setTag(holder);
        }

        /* Extract the city's object to show */
        Map.Entry<String, String> item = getItem(position);
        Holder holder = (Holder) convertView.getTag();

        /* Take the TextView from layout and set the city's name */
        holder.txtKey.setText(item.getKey());

        /* Take the TextView from layout and set the city's wiki link */
        holder.txtValue.setText(item.getValue());

        return convertView;
    }

    public class Holder {

        @InjectView(R.id.btnDelete)
        FloatingActionButton delete;
        @InjectView(R.id.etParamKey)
        EditText txtKey;
        @InjectView(R.id.etParamValue)
        EditText txtValue;

        public Holder(View view) {

            ButterKnife.inject(this, view);
        }
    }
}