package velasco.carlos.restclient.holders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.johnkil.print.PrintView;
import com.unnamed.b.atv.model.TreeNode;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.R;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRPostman;
import velasco.carlos.restclient.models.SRRequest;

public class IconTreeSRPostman<T extends SRPostman> {
    public boolean editMode;
    public int icon;
    public String id;
    public SRPostman<T> original;
    private TreeNode.BaseNodeViewHolder<IconTreeSRPostman<T>> holder;

    public IconTreeSRPostman(int icon, SRPostman<T> request) {
        this(icon, request, false);
    }

    public IconTreeSRPostman(int icon, SRPostman<T> model, boolean editMode) {
        this.icon = icon;
        this.id = model.getId();
        this.original = model;
        this.editMode = editMode;
    }

    public void bindViewHolder(TreeNode.BaseNodeViewHolder<IconTreeSRPostman<T>> iconTreeHolder, View view){
        //this.holder = iconTreeHolder;
        view.setTag(this);
    }

    public void updateModel(T model){
        this.original.getModel().update(model);
        //this.holder.createNodeView(node, this);
    }

    /* Holders */

    /**
     * Collection Holder
     */
    public static class IconTreeCollectionHolder extends TreeNode.BaseNodeViewHolder<IconTreeSRPostman<SRCollection>> {

        @InjectView(R.id.node_value)
        TextView tvValue;
        @InjectView(R.id.buttonsContainer)
        ViewGroup buttonsContainer;
        private PrintView arrowView;

        private TreeNode.TreeNodeClickListener folderClickListener;

        public IconTreeCollectionHolder(Context context, TreeNode.TreeNodeClickListener folderClickListener) {
            super(context);
            this.folderClickListener = folderClickListener;
        }

        @Override
        public View createNodeView(final TreeNode node, final IconTreeSRPostman<SRCollection> value) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.layout_icon_node_collection, null, false);

            ButterKnife.inject(this, view);

            value.bindViewHolder(this, view);

            tvValue.setText(value.original.getModel().getName());
            // edit buttons
            if (value.editMode) buttonsContainer.setVisibility(View.VISIBLE);
            else buttonsContainer.setVisibility(View.GONE);

            final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
            iconView.setIconText(context.getResources().getString(value.icon));

            arrowView = (PrintView) view.findViewById(R.id.arrow_icon);

            view.findViewById(R.id.btn_addFolder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IconTreeSRPostman<SRFolder> itemFolder = new IconTreeSRPostman<SRFolder>(R.string.ic_folder, SRFolder.createNewInstance(), value.editMode);
                    TreeNode newFolder = new TreeNode(itemFolder).setViewHolder(new IconTreeFolderHolder(context))
                            .setClickListener(folderClickListener);

                    getTreeView().addNode(node, newFolder);
                }
            });

            view.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Ask the user if they want to delete
                    new AlertDialog.Builder(getNodeView().getContext())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.delete)
                            .setMessage(R.string.really_delete)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // delete node
                                    getTreeView().removeNode(node);
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                }
            });

            //if My computer
        /*if (node.getLevel() == 1) {
            view.findViewById(R.id.btn_delete).setVisibility(View.GONE);
        }*/

            return view;
        }

        @Override
        public void toggle(boolean active) {
            arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
        }
    }

    public static class IconTreeFolderHolder extends TreeNode.BaseNodeViewHolder<IconTreeSRPostman<SRFolder>> {

        @InjectView(R.id.node_value)
        TextView tvValue;
        @InjectView(R.id.buttonsContainer)
        ViewGroup buttonsContainer;
        private PrintView arrowView;

        public IconTreeFolderHolder(Context context) {
            super(context);
        }

        @Override
        public View createNodeView(final TreeNode node, final IconTreeSRPostman<SRFolder> value) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.layout_icon_node_folder, null, false);

            ButterKnife.inject(this, view);
            value.bindViewHolder(this, view);

            tvValue.setText(value.original.getModel().getName());
            // edit buttons
            if (value.editMode) buttonsContainer.setVisibility(View.VISIBLE);
            else buttonsContainer.setVisibility(View.GONE);


            final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
            iconView.setIconText(context.getResources().getString(value.icon));

            view.findViewById(R.id.btn_addFolder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // add new folder
                    SRFolder folder = SRFolder.createNewInstance();
                    TreeNode newFolder = new TreeNode(new IconTreeSRPostman<SRFolder>(R.string.ic_folder, folder, value.editMode));
                    getTreeView().addNode(node, newFolder);
                }
            });

            view.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getTreeView().removeNode(node);
                }
            });

            // as a folder, can't add requests
            view.findViewById(R.id.btn_addFolder).setVisibility(View.GONE);

        /*if (node.isLeaf()) {
            arrowView.setVisibility(View.INVISIBLE);
        }*/
            arrowView = (PrintView) view.findViewById(R.id.arrow_icon);

            return view;
        }

        @Override
        public void toggle(boolean active) {
            arrowView.setIconText(context.getResources().getString(active ? R.string.ic_keyboard_arrow_down : R.string.ic_keyboard_arrow_right));
        }
    }

    public static class IconTreeRequestHolder extends TreeNode.BaseNodeViewHolder<IconTreeSRPostman<SRRequest>> {

        @InjectView(R.id.node_value)
        TextView tvValue;
        @InjectView(R.id.buttonsContainer)
        ViewGroup buttonsContainer;

        public IconTreeRequestHolder(Context context) {
            super(context);
        }

        @Override
        public View createNodeView(final TreeNode node, final IconTreeSRPostman<SRRequest> value) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view = inflater.inflate(R.layout.layout_icon_node_request, null, false);

            ButterKnife.inject(this, view);
            value.bindViewHolder(this, view);

            tvValue.setText(value.original.getModel().getName());
            // edit buttons
            if (value.editMode) buttonsContainer.setVisibility(View.VISIBLE);
            else buttonsContainer.setVisibility(View.GONE);

            final PrintView iconView = (PrintView) view.findViewById(R.id.icon);
            iconView.setIconText(context.getResources().getString(value.icon));

            switch (value.original.getModel().getMethod()) {
                case "GET":
                    iconView.setIconColor(context.getResources().getColor(R.color.colorMethodGET));
                    break;
                case "POST":
                    iconView.setIconColor(context.getResources().getColor(R.color.colorMethodPOST));
                    break;
                case "PUT":
                    iconView.setIconColor(context.getResources().getColor(R.color.colorMethodPUT));
                    break;
                case "DELETE":
                    iconView.setIconColor(context.getResources().getColor(R.color.colorMethodDELETE));
                    break;
            }

            view.findViewById(R.id.btn_addFolder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // empty behavior
                }
            });

            view.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getTreeView().removeNode(node);
                }
            });

            // as a request, can't add requests
            view.findViewById(R.id.btn_addFolder).setVisibility(View.GONE);

            return view;
        }

    }
}