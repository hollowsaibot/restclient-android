package velasco.carlos.restclient.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Carlos on 29/09/2015.
 */
public class SRParam implements Serializable {

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private String value;

    @SerializedName("type")
    private String type;

    @SerializedName("enabled")
    private boolean enabled;

    // ctor
    public SRParam(){
    }

    // Getters & Setters
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
