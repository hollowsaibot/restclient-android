package velasco.carlos.restclient.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Carlos on 29/09/2015.
 */
public class SRCollection extends SRRequestContainer<SRCollection> {

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("order")
    private ArrayList<String> order;

    @SerializedName("folders")
    private ArrayList<SRFolder> folders;

    @SerializedName("timestamp")
    private Number timestamp;

    @SerializedName("owner")
    private String owner;

    @SerializedName("remoteLink")
    private String remoteLink;

    @SerializedName("public")
    private Boolean publicCollecion;

    @SerializedName("requests")
    private ArrayList<SRRequest> requests;

    // ctor
    public SRCollection() {
        super();
        this.order = new ArrayList<>();
        this.folders = new ArrayList<>();
        this.requests = new ArrayList<>();
    }

    @Override
    public SRCollection getModel() {
        return this;
    }

    @Override
    public void update(SRCollection data) {
        this.setId(data.getId());
        this.name = data.name;
        this.description = data.description;
        this.order = data.order;
        this.folders = data.folders;
        this.timestamp = data.timestamp;
        this.owner = data.owner;
        this.remoteLink = data.remoteLink;
        this.publicCollecion = data.publicCollecion;
        this.requests = data.requests;
    }

    // Getters & Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<String> order) {
        this.order = order;
    }

    public ArrayList<SRFolder> getFolders() {
        return folders;
    }

    public void setFolders(ArrayList<SRFolder> folders) {
        this.folders = folders;
    }

    public Number getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Number timestamp) {
        this.timestamp = timestamp;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRemoteLink() {
        return remoteLink;
    }

    public void setRemoteLink(String remoteLink) {
        this.remoteLink = remoteLink;
    }

    public Boolean getPublicCollecion() {
        return publicCollecion;
    }

    public void setPublicCollecion(Boolean publicCollecion) {
        this.publicCollecion = publicCollecion;
    }

    public ArrayList<SRRequest> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<SRRequest> requests) {
        this.requests = requests;
    }

    /* Helper methods */

    /**
     * Get requests from collection in specified folder
     * @param collection
     * @param folder
     */
    public static ArrayList<SRRequest> getFolderRequests(SRCollection collection, SRFolder folder) {
        ArrayList<String> requiredRequests = new ArrayList<>();
        for(String requestId : folder.getOrder()){
            requiredRequests.add(requestId);
        }

        // get requests
        ArrayList<SRRequest> requests = new ArrayList<>();
        for(SRRequest request : collection.getRequests()) {
            if(requiredRequests.contains(request.getId())){
                requests.add(request);
            }
        }
        // return list
        return requests;
    }

    /**
     * Get all requests that are not in folders
     * @param collection
     * @return
     */
    public static ArrayList<SRRequest> getOrphanedRequests(SRCollection collection) {

        ArrayList<String> forbiddenRequests = new ArrayList<>();
        for(SRFolder folder : collection.getFolders()){
            for(String requestId : folder.getOrder()){
                forbiddenRequests.add(requestId);
            }
        }

        // get requests
        ArrayList<SRRequest> requests = new ArrayList<>();
        for(SRRequest request : collection.getRequests()) {
            if(!forbiddenRequests.contains(request.getId())){
                requests.add(request);
            }
        }
        // return list
        return requests;
    }

    public static SRCollection createNewInstanceFromData(SRCollection original, boolean withRelations) {
        SRCollection collection = new SRCollection();
        collection.setId(original.getId());
        collection.name = original.name;
        collection.description = original.description;
        collection.timestamp = original.timestamp;
        collection.owner = original.owner;
        collection.remoteLink = original.remoteLink;
        collection.publicCollecion = original.publicCollecion;
        if(withRelations) {
            collection.order.addAll(original.order);
            collection.folders.addAll(original.folders);
            collection.requests.addAll(original.requests);
        }
        // return collection
        return collection;
    }

    public boolean addFolderRequest(SRFolder folder, SRRequest request) {
        boolean done = false;
        for(SRFolder tmpFolder : this.getFolders()){
            if(tmpFolder.getId().equals(folder.getId())){
                this.getRequests().add(request);
                folder.getOrder().add(request.getId());
                done = true;
                break;
            }
        }
        return done;
    }

    public void addFolder(SRFolder folder) {
        folder.setOwner(this.getId());
        this.getFolders().add(folder);
    }

    public void addRequest(SRRequest request) {
        request.setCollectionId(this.getId());
        this.getOrder().add(request.getId());
        this.getRequests().add(request);
    }

    public void updateRequest(SRRequest request) {
        for(SRRequest r : getRequests()){
            if(r.getId().equals(request.getId())) {
                r.update(request);
            }
        }
    }
}
