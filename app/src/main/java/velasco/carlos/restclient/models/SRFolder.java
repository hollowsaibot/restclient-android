package velasco.carlos.restclient.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Carlos on 02/10/2015.
 */
public class SRFolder extends SRRequestContainer<SRFolder> {

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("order")
    private ArrayList<String> order;

    @SerializedName("owner")
    private String owner;

    // ctor
    public SRFolder(){
        super();
    }

    @Override
    public SRFolder getModel() {
        return this;
    }

    @Override
    public void update(SRFolder data) {
        this.name = data.name;
        this.description = data.description;
        this.order = data.order;
        this.owner = data.owner;
    }

    // getters & setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<String> order) {
        this.order = order;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        this.order = new ArrayList<>();
    }

    public static SRFolder createNewInstance() {
        SRFolder folder = new SRFolder();
        folder.setName("New Folder");
        return folder;
    }

    public static SRFolder createNewInstanceFromData(SRFolder original, boolean withRelations) {
        SRFolder folder = new SRFolder();
        folder.setId(original.getId());
        folder.name = original.name;
        folder.description = original.description;
        folder.owner = original.owner;
        if(withRelations) {
            folder.order = original.order;
        }
        return folder;
    }

    public void addRequest(SRCollection collection, SRRequest request) {
        collection.addFolderRequest(this, request);
    }
}
