package velasco.carlos.restclient.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Carlos on 29/09/2015.
 */
public abstract class SRPostman<T> implements Serializable {

    @SerializedName("id")
    private String id;

    public SRPostman(){
        this.id = UUID.randomUUID().toString();
    }

    // Getters & Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract T getModel();
    public abstract void update(T data);
}
