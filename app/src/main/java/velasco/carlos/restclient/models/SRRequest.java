package velasco.carlos.restclient.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos on 29/09/2015.
 */
public class SRRequest extends SRPostman<SRRequest> {

    @SerializedName("headers")
    private String headers;

    @SerializedName("url")
    private String url;

    @SerializedName("pathVariables")
    private Object pathVariables;

    @SerializedName("preRequestScript")
    private String preRequestScript;

    @SerializedName("method")
    private String method;

    @SerializedName("collectionId")
    private String collectionId;

    @SerializedName("data")
    private List<SRParam> data;

    @SerializedName("dataMode")
    private String dataMode;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("descriptionFormat")
    private String descriptionFormat;

    @SerializedName("time")
    private Number time;

    @SerializedName("version")
    private Integer version;

    @SerializedName("responses")
    private List<Object> responses;

    @SerializedName("tests")
    private String tests;

    @SerializedName("currentHelper")
    private String currentHelper;

    @SerializedName("helperAttributes")
    private Object helperAttributes;

    @SerializedName("rawModeData")
    private String rawModeData;

    // ctor
    public SRRequest(){
        super();
        this.method = "GET";
        this.data = new ArrayList<>();
        this.responses = new ArrayList<>();
    }

    @Override
    public SRRequest getModel() {
        return this;
    }

    @Override
    public void update(SRRequest data) {
        this.headers = data.headers;
        this.url = data.url;
        this.pathVariables = data.pathVariables;
        this.preRequestScript = data.preRequestScript;
        this.method = data.method;
        this.collectionId = data.collectionId;
        this.data = data.data;
        this.dataMode = data.dataMode;
        this.name = data.name;
        this.description = data.description;
        this.descriptionFormat = data.descriptionFormat;
        this.time = data.time;
        this.version = data.version;
        this.responses = data.responses;
        this.tests = data.tests;
        this.currentHelper = data.currentHelper;
        this.helperAttributes = data.helperAttributes;
        this.rawModeData = data.rawModeData;
    }

    // Getters & Setters
    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getPathVariables() {
        return pathVariables;
    }

    public void setPathVariables(Object pathVariables) {
        this.pathVariables = pathVariables;
    }

    public String getPreRequestScript() {
        return preRequestScript;
    }

    public void setPreRequestScript(String preRequestScript) {
        this.preRequestScript = preRequestScript;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public List<SRParam> getData() {
        return data;
    }

    public void setData(List<SRParam> data) {
        this.data = data;
    }

    public String getDataMode() {
        return dataMode;
    }

    public void setDataMode(String dataMode) {
        this.dataMode = dataMode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionFormat() {
        return descriptionFormat;
    }

    public void setDescriptionFormat(String descriptionFormat) {
        this.descriptionFormat = descriptionFormat;
    }

    public Number getTime() {
        return time;
    }

    public void setTime(Number time) {
        this.time = time;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public List<Object> getResponses() {
        return responses;
    }

    public void setResponses(List<Object> responses) {
        this.responses = responses;
    }

    public String getTests() {
        return tests;
    }

    public void setTests(String tests) {
        this.tests = tests;
    }

    public String getCurrentHelper() {
        return currentHelper;
    }

    public void setCurrentHelper(String currentHelper) {
        this.currentHelper = currentHelper;
    }

    public Object getHelperAttributes() {
        return helperAttributes;
    }

    public void setHelperAttributes(Object helperAttributes) {
        this.helperAttributes = helperAttributes;
    }

    public String getRawModeData() {
        return rawModeData;
    }

    public void setRawModeData(String rawModeData) {
        this.rawModeData = rawModeData;
    }
}
