package velasco.carlos.restclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.models.SRPostman;
import velasco.carlos.restclient.models.SRRequest;

public class EditorRequestActivity extends AppCompatActivity {

    private EditorRequestActivity self = this;
    private SRRequest model;

    @InjectView(R.id.etRequestData)
    EditText etRequestData;

    @InjectView(R.id.etRequestDataMode)
    EditText etRequestDataMode;

    @InjectView(R.id.etRequestDescription)
    EditText etRequestDescription;

    @InjectView(R.id.etRequestDescriptionFormat)
    EditText etRequestDescriptionFormat;

    @InjectView(R.id.etRequestHeaders)
    EditText etRequestHeaders;

    @InjectView(R.id.etRequestMethod)
    EditText etRequestMethod;

    @InjectView(R.id.etRequestName)
    EditText etRequestName;

    @InjectView(R.id.etRequestRawModeData)
    EditText etRequestRawModeData;

    @InjectView(R.id.etRequestUrl)
    EditText etRequestUrl;

    @InjectView(R.id.btnSaveRequest)
    FloatingActionButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_request);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(getTitle());

        ButterKnife.inject(this);

        model = getModel(this);

        loadViewFromModel(model);

        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateModelFromView(model);
                SoftKeyboardHelper.hideKeyboard(self);

                Snackbar.make(view, getString(R.string.editor_request_saved_message), Snackbar.LENGTH_LONG)
                        .setAction(R.string.snackbar_action_done, clickListener).show();
            }
        });
    }

    private void loadViewFromModel(SRRequest model) {

        //etRequestData.setText(model.getData());
        etRequestDataMode.setText(model.getDataMode());
        etRequestDescription.setText(model.getDescription());
        etRequestDescriptionFormat.setText(model.getDescriptionFormat());
        etRequestHeaders.setText(model.getHeaders());
        etRequestMethod.setText(model.getMethod());
        etRequestName.setText(model.getName());
        etRequestRawModeData.setText(model.getRawModeData());
        etRequestUrl.setText(model.getUrl());
    }

    private void updateModelFromView(SRRequest model) {
        //model.setData(etRequestData.getText().toString());
        model.setDataMode(etRequestDataMode.getText().toString());
        model.setDescription(etRequestDescription.getText().toString());
        model.setDescriptionFormat(etRequestDescriptionFormat.getText().toString());
        model.setHeaders(etRequestHeaders.getText().toString());
        model.setMethod(etRequestMethod.getText().toString());
        model.setName(etRequestName.getText().toString());
        model.setRawModeData(etRequestRawModeData.getText().toString());
        model.setUrl(etRequestUrl.getText().toString());

        // set status
        Intent resultIntent = new Intent();
        resultIntent.putExtra(SRPostman.class.getName(), model);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    private SRRequest getModel(Activity activity) {
        Intent intent = activity.getIntent();
        SRRequest tmpRequest = (SRRequest) intent.getSerializableExtra(SRRequest.class.getName());
        if(tmpRequest == null) {
            tmpRequest = new SRRequest();
        }
        // return model
        return tmpRequest;
    }
}
