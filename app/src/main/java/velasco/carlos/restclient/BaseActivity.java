package velasco.carlos.restclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.github.johnkil.print.PrintConfig;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.fabric.sdk.android.Fabric;
import velasco.carlos.restclient.fragments.NavigationDrawerFragment;
import velasco.carlos.restclient.fragments.RequestFragment;
import velasco.carlos.restclient.fragments.ResponseFragment;
import velasco.carlos.restclient.helpers.SRCollectionsManager;
import velasco.carlos.restclient.models.SRRequest;

public abstract class BaseActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        RequestFragment.RestCallbackInterface {

    private BaseActivity self = this;

    // Operations
    public static final int OPERATION_IMPORT = 0;
    public static final int OPERATION_MANAGEMENT = 1;

    // loaded from array
    protected String[] titles;

    private ViewPagerAdapter pagerAdapter;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @InjectView(R.id.tabs)
    TabLayout tabLayout;

    @InjectView(R.id.pager)
    ViewPager viewPager;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    protected abstract void setupCotentView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build(), new Crashlytics());
        PrintConfig.initDefault(getAssets(), "fonts/material-icon-font.ttf");
        SRCollectionsManager.start(this);

        // initialize layout
        setupCotentView();

        ButterKnife.inject(this);

        // Set a toolbar to replace the action bar.
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        // Get navigation fragment
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer, drawerLayout, toolbar);

        // init view pager
        initViewPager(savedInstanceState);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        /*FragmentManager fragmentManager = getSupportFragmentManager();
        // clear fragments stack
        for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
        // choose option
        switch (position) {
            case 0:
            break;
        }
        */
    }

    @Override
    public void onNavigationDrawerRequestSelected(final SRRequest request) {
        // propage submit
        viewPager.setCurrentItem(0);
        self.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pagerAdapter.loadRequest(request);
            }
        });
    }


    @Override
    public void onNavigationDrawerRaiseAction(int action) {
        switch (action) {
            case R.id.action_import:
                openImporterActivity();
                break;
            case R.id.action_management:
                openManagementActivity();
                break;
        }
    }

    @Override
    public void notifyResponse(String header, String response) {
        viewPager.setCurrentItem(1); // response
        pagerAdapter.updateResponse(header, response);
    }

    @Override
    public void collectionUpdated() {
        // reload items
        mNavigationDrawerFragment.loadNavigationMenuItems();
    }


    public void openImporterActivity() {
        Intent intent = new Intent(self, ImportActivity.class);
        startActivityForResult(intent, OPERATION_IMPORT);
    }

    public void openManagementActivity() {
        Intent intent = new Intent(self, ManagementActivity.class);
        startActivityForResult(intent, OPERATION_MANAGEMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case OPERATION_IMPORT:
                case OPERATION_MANAGEMENT:
                    // reload items
                    mNavigationDrawerFragment.loadNavigationMenuItems();
                    break;
            }
        }
    }

    /* PRIVATE METHODS */
    private void initViewPager(Bundle savedInstanceState) {
        titles = getResources().getStringArray(R.array.tab_titles);

        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), savedInstanceState);
        viewPager.setAdapter(pagerAdapter);

        // create the TabHost that will contain the Tabs
        for (int i = 0; i < titles.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(titles[i]));
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        pagerAdapter.onSaveInstanceState(outState);
    }

    /* PRIVATE METHODS */
    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private RequestFragment requestFragment;
        private ResponseFragment responseFragment;
        private FragmentManager fm;

        public ViewPagerAdapter(FragmentManager fm, Bundle savedInstanceState) {
            super(fm);
            this.fm = fm;
            if(savedInstanceState != null){
                requestFragment = (RequestFragment) fm.getFragment(savedInstanceState, RequestFragment.class.getSimpleName());
                responseFragment = (ResponseFragment) fm.getFragment(savedInstanceState, ResponseFragment.class.getSimpleName());
            } else {
                requestFragment = new RequestFragment();
                responseFragment = new ResponseFragment();
            }
        }

        public void onSaveInstanceState(Bundle outState){
            if(requestFragment != null && responseFragment != null) {
                fm.putFragment(outState, RequestFragment.class.getSimpleName(), requestFragment);
                fm.putFragment(outState, ResponseFragment.class.getSimpleName(), responseFragment);
            } else {
                Log.d("ViewPager", "No initialized fragments for save");
            }
        }

        public Fragment getItem(int num) {

            Fragment fragment = null;
            switch (num) {
                case 0: // request
                    fragment = requestFragment;
                    break;
                case 1: // response
                    fragment = responseFragment;
                    break;
                default: // request
                    Log.e("ViewPagerAdapter", "Number of fragments incorrect");
            }
            return fragment;
        }

        public void loadRequest(final SRRequest request) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(SRRequest.class.getName(), request);
            Intent intent = new Intent();
            intent.putExtras(bundle);
            requestFragment.onActivityResult(RequestFragment.CODE_LOAD_REQUEST, Activity.RESULT_OK, intent);
        }

        public void updateResponse(final String header, final String response) {
            responseFragment.updateResponseFragment(header, response);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
