package velasco.carlos.restclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRPostman;

public class EditorFolderActivity extends AppCompatActivity {

    private EditorFolderActivity self = this;
    private SRFolder model;

    @InjectView(R.id.etFolderName)
    EditText etFolderName;

    @InjectView(R.id.etFolderDescription)
    EditText etFolderDescription;

    @InjectView(R.id.btnSaveFolder)
    FloatingActionButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_folder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(getTitle());

        ButterKnife.inject(this);

        model = getModel(this);

        loadViewFromModel(model);

        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateModelFromView(model);
                SoftKeyboardHelper.hideKeyboard(self);

                Snackbar.make(view, getString(R.string.editor_folder_saved_message), Snackbar.LENGTH_LONG)
                        .setAction(R.string.snackbar_action_done, clickListener).show();
            }
        });
    }

    private void loadViewFromModel(SRFolder model) {
        etFolderName.setText(model.getName());
        etFolderDescription.setText(model.getDescription());
    }

    private void updateModelFromView(SRFolder model) {
        model.setName(etFolderName.getText().toString());
        model.setDescription(etFolderDescription.getText().toString());

        // set status
        Intent resultIntent = new Intent();
        resultIntent.putExtra(SRPostman.class.getName(), model);
        setResult(Activity.RESULT_OK, resultIntent);
    }

    private SRFolder getModel(Activity activity) {
        Intent intent = activity.getIntent();
        SRFolder tmpFolder = (SRFolder) intent.getSerializableExtra(SRFolder.class.getName());
        if(tmpFolder == null) {
            tmpFolder = new SRFolder();
        }
        // return model
        return tmpFolder;
    }
}
