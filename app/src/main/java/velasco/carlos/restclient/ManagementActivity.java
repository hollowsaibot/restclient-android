package velasco.carlos.restclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.fragments.SnackLayoutFragment;
import velasco.carlos.restclient.helpers.SRCollectionsManager;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.helpers.TreeNodeCollectionHelper;
import velasco.carlos.restclient.holders.IconTreeSRPostman;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRPostman;
import velasco.carlos.restclient.models.SRRequest;

public class ManagementActivity extends AppCompatActivity implements SnackLayoutFragment.SnackLayoutCallbacks {

    private static final int CODE_EDIT_COLLECTION = 0;
    private static final int CODE_EDIT_FOLDER = 1;
    private static final int CODE_EDIT_REQUEST = 2;
    private static final String TAG_SAVE_STATE_COLLECTIONS = "TAG_SAVE_STATE_COLLECTIONS";

    private ManagementActivity self = this;
    private static final String TAG = "ManagementActivity";
    
    @InjectView(R.id.treeViewContainer)
    ViewGroup treeContainerView;
    @InjectView(R.id.btnExpandAll)
    com.github.johnkil.print.PrintView btnExpandAll;
    @InjectView(R.id.btnCollapseAll)
    com.github.johnkil.print.PrintView btnCollapseAll;

    private AndroidTreeView tView;
    private TreeNode root;
    private TreeNodeCollectionHelper helper;
    private List<SRCollection> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.inject(this);

        btnExpandAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tView.expandAll();
            }
        });

        btnCollapseAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tView.collapseAll();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnManagementInfo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                SoftKeyboardHelper.hideKeyboard(self);
                ArrayList<SRCollection> collections = helper.parseTreeNodeToItem(root);
                if(collections.size() > 0) {
                    View.OnClickListener onSave = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    SRCollectionsManager.putCollections(collections);
                    Snackbar.make(view, getString(R.string.management_updated_message), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.snackbar_action_done), onSave).show();
                    // set as ok
                    setResult(RESULT_OK);

                } else {
                    SRCollectionsManager.putCollections(collections);
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });

        if(savedInstanceState != null){
            list = (ArrayList<SRCollection>) savedInstanceState.getSerializable(TAG_SAVE_STATE_COLLECTIONS);
        } else {
            list = SRCollectionsManager.getCollections();
        }

        loadTreeNodeData(list);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<SRCollection> collections = helper.parseTreeNodeToItem(root);
        outState.putSerializable(TAG_SAVE_STATE_COLLECTIONS, collections);
    }

    private void loadTreeNodeData(List<SRCollection> list) {

        helper = new TreeNodeCollectionHelper();
        root = helper.convertItemToTreeNode(this, list, collectionClickListener, folderClickListener, requestClickListener, true);
        tView = new AndroidTreeView(self, root);

        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        //tView.setDefaultViewHolder(IconTreeFolderHolder.class);
        //tView.setDefaultNodeClickListener(nodeClickListener);
        //tView.setDefaultNodeLongClickListener(nodeLongClickListener);

        treeContainerView.removeAllViews();
        treeContainerView.addView(tView.getView());
    }

    // NAV TREE VIEW MENU
    private TreeNode.TreeNodeClickListener collectionClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            IconTreeSRPostman<SRCollection> item = (IconTreeSRPostman<SRCollection>) value;

            SRCollection object = item.original.getModel();
            Intent intent = new Intent(self, EditorCollectionActivity.class);
            intent.putExtra(SRCollection.class.getName(), object);
            startActivityForResult(intent, CODE_EDIT_COLLECTION);

            // avoid collapse/expand
            node.setExpanded(!node.isExpanded());
        }
    };

    private TreeNode.TreeNodeClickListener folderClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            IconTreeSRPostman<SRFolder> item = (IconTreeSRPostman<SRFolder>) value;

            SRFolder object = item.original.getModel();
            Intent intent = new Intent(self, EditorFolderActivity.class);
            intent.putExtra(SRFolder.class.getName(), object);
            startActivityForResult(intent, CODE_EDIT_FOLDER);

            // avoid collapse/expand
            node.setExpanded(!node.isExpanded());
        }
    };

    private TreeNode.TreeNodeClickListener requestClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {
            IconTreeSRPostman<SRRequest> item = (IconTreeSRPostman<SRRequest>) value;

            SRRequest object = item.original.getModel();
            Intent intent = new Intent(self, EditorRequestActivity.class);
            intent.putExtra(SRRequest.class.getName(), object);
            startActivityForResult(intent, CODE_EDIT_REQUEST);

            // avoid collapse/expand
            node.setExpanded(!node.isExpanded());
        }
    };

    private TreeNode.TreeNodeLongClickListener nodeLongClickListener = new TreeNode.TreeNodeLongClickListener() {
        @Override
        public boolean onLongClick(TreeNode node, Object value) {
            IconTreeSRPostman<SRFolder> item = (IconTreeSRPostman<SRFolder>) value;
            // TODO: replace with your own code
            Toast.makeText(self, "Long click: " + item.original.getModel().getName(), Toast.LENGTH_SHORT).show();
            return true;
        }
    };

    @Override
    public void onSnackLayoutItemDone(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK){
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            SRPostman target = (SRPostman) data.getExtras().getSerializable(SRPostman.class.getName());
            // redraw
            helper.updateItemOnTreeNode(root, target);
            loadTreeNodeData(list);

            // update
            switch (resultCode) {
                case CODE_EDIT_COLLECTION:
                    Toast.makeText(this, "collection", Toast.LENGTH_SHORT).show();
                    break;
                case CODE_EDIT_FOLDER:
                    Toast.makeText(this, "folder", Toast.LENGTH_SHORT).show();
                    break;
                case CODE_EDIT_REQUEST:
                    Toast.makeText(this, "request", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        }
    }
}
