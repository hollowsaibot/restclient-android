package velasco.carlos.restclient.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.R;
import velasco.carlos.restclient.adapters.RequestContainerListAdapter;
import velasco.carlos.restclient.helpers.SRCollectionsManager;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRRequest;

/**
 * Created by Carlos on 20/10/2015.
 */
public class RequestContainerPickerDialog extends DialogFragment implements AdapterView.OnItemClickListener{

    private RequestContainerPickerCallback mCallbacks;

    public interface RequestContainerPickerCallback{
        void onRequestAdded(SRRequest request);
    }

    /* collection selector */
    @InjectView(R.id.folderPickerListView)
    ListView mFoldersList;

    /* collection creator */
    @InjectView(R.id.etCollectionName)
    EditText etCollectionName;
    @InjectView(R.id.etRequestName)
    EditText etRequestName;

    @InjectView(R.id.tvCollectionName)
    TextView tvCollectionName;
    @InjectView(R.id.tvRequestName)
    TextView tvRequestName;

    @InjectView(R.id.etRequestDescription)
    EditText etRequestDescription;

    @InjectView(R.id.btnAddToCollection)
    Button btnAddToCollection;

    List<SRCollection> mCollections = new ArrayList<>();
    private SRRequest request;
    private ArrayList<RequestContainerListAdapter.SRRequestContainerItem> items = new ArrayList<>();

    public static RequestContainerPickerDialog newInstance(SRRequest request) {
        RequestContainerPickerDialog dialog = new RequestContainerPickerDialog();
        Bundle args = new Bundle();
        args.putSerializable(SRRequest.class.getName(), request);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // set title
        getDialog().setTitle(getString(R.string.pick_folder_destination));

        View v = inflater.inflate(R.layout.dialog_collection_folder_picker, container,
                true);

        request = (SRRequest) getArguments().getSerializable(SRRequest.class.getName());
        if(TextUtils.isEmpty(request.getName())){
            request.setName(request.getUrl());
        }

        // load views
        ButterKnife.inject(this, v);

        String requiredText = getString(R.string.required_text);
        for(TextView requiredLabel : Arrays.asList(tvCollectionName, tvRequestName)){
            requiredLabel.setText(requiredLabel.getText().toString() + requiredText);
        }

        // load collections from prefs
        mCollections = SRCollectionsManager.getCollections();

        items.addAll(RequestContainerListAdapter.SRRequestContainerItem.convertCollectionsToItems(mCollections));

        final RequestContainerListAdapter adapter = new RequestContainerListAdapter(
                    getContext(), items);

        mFoldersList.setOnItemClickListener(this);
        mFoldersList.setAdapter(adapter);

        btnAddToCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SoftKeyboardHelper.hideKeyboard(getDialog().getOwnerActivity());
                // validate fields
                String collectionName = etCollectionName.getText().toString();
                String requestName = etRequestName.getText().toString();
                String requestDescription = etRequestDescription.getText().toString();

                if(collectionName.length() > 0 && requestName.length() > 0) {
                    SRCollection collection = new SRCollection();
                    collection.setName(collectionName);
                    request.setName(requestName);
                    request.setDescription(requestDescription);

                    collection.addRequest(request);

                    // put data
                    mCollections.add(collection);
                    SRCollectionsManager.putCollections(mCollections);

                    // activity feedback
                    collectionUpdatedFeedback();
                    // close dialog
                    dismiss();
                } else {
                    // show required fields
                    Toast.makeText(getActivity(), getString(R.string.error_required_fields), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mCallbacks = (RequestContainerPickerCallback) activity;
        }
        catch (ClassCastException ex){
            Log.d(RequestContainerPickerDialog.class.getName(), "Dialog not attached to an Activity");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        RequestContainerListAdapter.SRRequestContainerItem item = items.get(i);
        boolean found = false;
        if (item.getIsCollection()) {
            for (SRCollection collection : mCollections) {
                if (collection.getId().equals(item.getCollectionId())) {
                    collection.addRequest(request);
                    found = true;
                }
                if(found) break;
            }
        } else {

            for (SRCollection collection : mCollections) {
                if (collection.getId().equals(item.getCollectionId())) {
                    for (SRFolder folder : collection.getFolders()) {
                        if (folder.getId().equals(item.getFolderId())) {
                            folder.addRequest(collection, request);
                            found = true;
                        }
                        if(found) break;
                    }
                }
                if(found) break;
            }
        }
        // save
        SRCollectionsManager.putCollections(mCollections);

        // activity feedback
        collectionUpdatedFeedback();
        // close dialog
        dismiss();
    }

    private void collectionUpdatedFeedback() {
        if(mCallbacks != null){
            mCallbacks.onRequestAdded(request);
        }
        // fragment feedback
        if(getTargetFragment() != null){
            Intent data = new Intent();
            data.putExtra(SRRequest.class.getName(), request);
            int targetRequestCode = getTargetRequestCode();
            getTargetFragment().onActivityResult(targetRequestCode, Activity.RESULT_OK, data);
        }
    }
}
