package velasco.carlos.restclient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import velasco.carlos.restclient.R;
import velasco.carlos.restclient.adapters.ParamListAdapter;
import velasco.carlos.restclient.helpers.MapEntry;

/**
 * Created by Carlos on 29/03/2015.
 */
public class AddHeaderDialog {

    EditText etName;
    EditText etValue;
    EditText etUsername;
    EditText etPassword;

    public void openModalHeader(final Activity context, final ParamListAdapter adapterHeaders, final Dialog.OnClickListener callback) {

        // custom dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.title_add_header));
        builder.setCancelable(false);

        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_add_param, null);

        builder.setView(dialoglayout);

        List<String> list = new ArrayList<String>() {{
            addAll(getHeaders(context).keySet());
        }};
        Collections.sort(list);
        final Spinner sp = (Spinner) dialoglayout.findViewById(R.id.add_header_type);
        ArrayAdapter<String> adp = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, list);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adp);

        final DialogInterface.OnClickListener btnOk = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
                    /*dialog.cancel();*/

                String tag = sp.getSelectedItem().toString();
                switch (tag)
                {
                    case "Custom":
                        Map.Entry<String, String> entry = new MapEntry<String, String>(etName.getText().toString(), etValue.getText().toString());
                        adapterHeaders.add(entry);
                        break;
                    case "Accept":
                    case "Authorization":
                    case "Content-Type":
                    case "Cookie":
                    case "User-Agent":
                        adapterHeaders.add(new MapEntry(tag, etValue.getText().toString()));
                        break;
                    case "User-Agent (Android)":
                    case "User-Agent (iPhone)":
                    case "User-Agent (iPad)":
                        adapterHeaders.add(new MapEntry("User-Agent", etValue.getText().toString()));
                        break;
                    case "Authorization (Basic)":
                        String creds = String.format("%s:%s",etUsername.getText().toString(),etPassword.getText().toString());
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        adapterHeaders.add(new MapEntry("Authorization", auth));
                        break;
                }
                callback.onClick(dialog, which);
            }
        };

        // callback
        builder.setPositiveButton(context.getResources().getString(R.string.txtOK), btnOk);
        builder.setNegativeButton(context.getResources().getString(R.string.txtCancel), null);

        final AlertDialog dialog = builder.create();

        etName = (EditText) dialoglayout.findViewById(R.id.etHeaderName);
        etValue = (EditText) dialoglayout.findViewById(R.id.etHeaderValue);
        etUsername = (EditText) dialoglayout.findViewById(R.id.etHeaderUsername);
        etPassword = (EditText) dialoglayout.findViewById(R.id.etHeaderPassword);

        final HashMap<String, View> controlRows = new HashMap<>();
        controlRows.put("rowName", (TableRow) dialoglayout.findViewById(R.id.rowName));
        controlRows.put("rowValue", (TableRow) dialoglayout.findViewById(R.id.rowValue));
        controlRows.put("rowUsername", (TableRow) dialoglayout.findViewById(R.id.rowUsername));
        controlRows.put("rowPassword", (TableRow) dialoglayout.findViewById(R.id.rowPassword));

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Object item = sp.getSelectedItem();
                boolean enabledOk = validateControls(item.toString());
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(enabledOk);
            }

            private boolean validateControls(String tag) {
                boolean validate = false;

                switch (tag) {
                    case "Custom":
                        validate = etName.getText().length() > 0 && etValue.getText().length() > 0;
                        break;
                    case "Accept":
                    case "Authorization":
                    case "Content-Type":
                    case "Cookie":
                    case "User-Agent":
                    case "User-Agent (Android)":
                    case "User-Agent (iPhone)":
                    case "User-Agent (iPad)":
                        validate = etValue.getText().length() > 0;
                        break;
                    case "Authorization (Basic)":
                        validate = etUsername.getText().length() > 0 && etPassword.getText().length() > 0;
                        break;
                }
                return validate;
            }
        };

        etName.addTextChangedListener(textWatcher);
        etValue.addTextChangedListener(textWatcher);
        etUsername.addTextChangedListener(textWatcher);
        etPassword.addTextChangedListener(textWatcher);

        HashMap<String, String> defaultValues = new HashMap<>();
        defaultValues.put("User-Agent (Android)", context.getResources().getString(R.string.user_agent_android));
        defaultValues.put("User-Agent (iPhone)", context.getResources().getString(R.string.user_agent_iphone));
        defaultValues.put("User-Agent (iPad)", context.getResources().getString(R.string.user_agent_ipad));

        sp.setOnItemSelectedListener(validateItemSelected(dialog, controlRows, defaultValues));

        dialog.show();

        // enable / disable
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false); //BUTTON1 is positive button
    }

    private AdapterView.OnItemSelectedListener validateItemSelected(final AlertDialog dialog, final HashMap<String, View> inputs, final HashMap<String, String> defaultValues) {

        return new AdapterView.OnItemSelectedListener() {

            private void updateControls(String tag, HashMap<String, View> inputs) {

                etName.setText("");
                etValue.setText("");
                etUsername.setText("");
                etPassword.setText("");

                ((TableRow) inputs.get("rowName")).setVisibility(View.GONE);
                ((TableRow) inputs.get("rowValue")).setVisibility(View.GONE);
                ((TableRow) inputs.get("rowUsername")).setVisibility(View.GONE);
                ((TableRow) inputs.get("rowPassword")).setVisibility(View.GONE);

                switch (tag)
                {
                    case "Custom":
                        ((TableRow) inputs.get("rowName")).setVisibility(View.VISIBLE);
                        ((TableRow) inputs.get("rowValue")).setVisibility(View.VISIBLE);
                        break;
                    case "Accept":
                    case "Authorization":
                    case "Content-Type":
                    case "Cookie":
                    case "User-Agent":
                    case "User-Agent (Android)":
                    case "User-Agent (iPhone)":
                    case "User-Agent (iPad)":
                        ((TableRow) inputs.get("rowValue")).setVisibility(View.VISIBLE);
                        // set default value
                        if(defaultValues.containsKey(tag)) {
                            etValue.setText(defaultValues.get(tag));
                        }
                        break;
                    case "Authorization (Basic)":
                        ((TableRow) inputs.get("rowUsername")).setVisibility(View.VISIBLE);
                        ((TableRow) inputs.get("rowPassword")).setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                // set visibility to inputs
                updateControls(item.toString(), inputs);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            }
        };
    }

    private Map<String, HeaderConfig> getHeaders(Context context){
        HashMap<String, HeaderConfig> headers = new HashMap<>();
        headers.put("Custom", new HeaderConfig(true, true, "", ""));
        headers.put("Accept", new HeaderConfig(false, true, "", ""));
        headers.put("Authorization", new HeaderConfig(false, true, "", ""));
        headers.put("Authorization (Basic)", new HeaderConfig(true));
        headers.put("Content-Type", new HeaderConfig(false, true, "", ""));
        headers.put("Cookie", new HeaderConfig(false, true, "", ""));
        headers.put("User-Agent", new HeaderConfig(false, true, "", ""));
        headers.put("User-Agent (Android)", new HeaderConfig(true, true, "", context.getString(R.string.user_agent_android)));
        headers.put("User-Agent (iPhone)", new HeaderConfig(true, true, "", context.getString(R.string.user_agent_iphone)));
        headers.put("User-Agent (iPad)", new HeaderConfig(true, true, "", context.getString(R.string.user_agent_ipad)));

        return headers;
    }

    private static class HeaderConfig {
        private boolean hasKey = false;
        private boolean hasValue = false;
        private String defaultKey = "";
        private String defaultValue = "";
        protected boolean isAuth = false;

        public HeaderConfig(boolean hasKey, boolean hasValue, String defaultKey, String defaultValue){
            this.hasKey = hasKey;
            this.hasValue = hasValue;
            this.defaultKey = defaultKey;
            this.defaultValue = defaultValue;
        }

        public HeaderConfig(boolean isAuth){
            this.isAuth = isAuth;
        }

            /* Getters & Setters */

        public boolean isAuth() {
            return isAuth;
        }

        public void setAuth(boolean isAuth) {
            this.isAuth = isAuth;
        }

        public boolean getHasKey() {
            return hasKey;
        }

        public void setHasKey(boolean hasKey) {
            this.hasKey = hasKey;
        }

        public boolean getHasValue() {
            return hasValue;
        }

        public void setHasValue(boolean hasValue) {
            this.hasValue = hasValue;
        }

        public String getDefaultKey() {
            return defaultKey;
        }

        public void setDefaultKey(String defaultKey) {
            this.defaultKey = defaultKey;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }
    }
}
