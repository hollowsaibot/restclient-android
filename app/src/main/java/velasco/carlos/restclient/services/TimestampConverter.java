package velasco.carlos.restclient.services;

import java.io.IOException;
import java.sql.Timestamp;

import retrofit.Converter;

/**
 * Created by Carlos on 30/09/2015.
 */
public class TimestampConverter implements Converter<Integer, Timestamp> {

    @Override
    public Timestamp convert(Integer value) throws IOException {
        return new Timestamp(value);
    }
}
