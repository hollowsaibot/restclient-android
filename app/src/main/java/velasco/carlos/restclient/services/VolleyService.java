package velasco.carlos.restclient.services;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import velasco.carlos.restclient.NetworkResponseRequest;

public class VolleyService {

    public interface VolleyServiceCallback<K> {
        void onSuccess(K response);

        void onError(K message);
    }

    public interface StringServiceCallback extends VolleyServiceCallback<String> {
        void onSuccess(String response);

        void onSuccess(String response, NetworkResponse network);

        void onError(String message);
    }

    public interface JsonServiceCallback extends VolleyServiceCallback<JSONObject> {
        void onSuccess(JSONObject response);

        void onError(String message);
    }

    public Map<String, Integer> getMethods() {
        HashMap<String, Integer> methods = new HashMap<>();
        methods.put("GET", Request.Method.GET);
        methods.put("POST", Request.Method.POST);
        methods.put("PUT", Request.Method.PUT);
        methods.put("DELETE", Request.Method.DELETE);
        methods.put("HEAD", Request.Method.HEAD);
        methods.put("TRACE", Request.Method.TRACE);
        methods.put("OPTIONS", Request.Method.OPTIONS);
        return methods;
    }

    public String getMethodName(int option) {
        switch (option) {
            case Request.Method.GET:
                return "GET";

            case Request.Method.POST:
                return "POST";

            case Request.Method.PUT:
                return "PUT";

            case Request.Method.DELETE:
                return "DELETE";

            case Request.Method.HEAD:
                return "HEAD";

            case Request.Method.TRACE:
                return "TRACE";

            case Request.Method.OPTIONS:
                return "OPTIONS";

            default:
                throw new InvalidParameterException();
        }

    }

    public StringRequest sendStringRequest(int method, String url, final Map<String, String> params, final StringServiceCallback callback) {

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(method, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }

        }) {

            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        // return
        return stringRequest;
    }

    public StringRequest sendStringRequest(int method, String url, final Map<String, String> headers, boolean nil, final StringServiceCallback callback) {

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(method, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        // return
        return stringRequest;
    }

    public StringRequest sendStringRequest(int method, String url, final Map<String, String> headers, final Map<String, String> params, final StringServiceCallback callback) {

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(method, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        // return
        return stringRequest;
    }

    public JsonObjectRequest sendJsonObjectRequest(int method, String url, final Map<String, String> params, final JsonServiceCallback callback) {

        // Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(method, url, new JSONObject(params),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        // return
        return stringRequest;
    }

    public JsonObjectRequest sendJsonObjectRequest(int method, String url, final Map<String, String> headers, boolean nil, final JsonServiceCallback callback) {

        // Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(method, url, new JSONObject(),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        // return
        return stringRequest;
    }

    public JsonObjectRequest sendJsonObjectRequest(int method, String url, final Map<String, String> headers, final Map<String, String> params, final JsonServiceCallback callback) {

        // Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(method, url, new JSONObject(params),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        callback.onSuccess(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        // return
        return stringRequest;
    }

    public NetworkResponseRequest sendNetworkResponseRequest(int method, String url, final Map<String, String> headers, final Map<String, String> params, final StringServiceCallback callback) {
        NetworkResponseRequest request = new NetworkResponseRequest(method, url,
                headers,
                params,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        // This is status code: response.statusCode
                        // This is string response: NetworkResponseRequest.parseToString(response)
                        String body = "";
                        try {
                            body = new String(response.data, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        callback.onSuccess(body, response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
            }
        }
        );
        return request;
    }
}