package velasco.carlos.restclient.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Timestamp;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.GET;
import retrofit.http.Path;
import velasco.carlos.restclient.models.SRCollection;

/**
 * Created by Carlos on 29/09/2015.
 */
public class SRService {

    public interface PostmanService {
        @GET("/collections/{collectionId}")
        Call<SRCollection> getCollection(@Path("collectionId") String collectionId);
    }

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.getpostman.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private PostmanService service = retrofit.create(PostmanService.class);

    public PostmanService getPostmanService(){
        return service;
    }

}
