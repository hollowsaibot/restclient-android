package velasco.carlos.restclient.helpers;

import android.content.Context;
import android.view.View;

import com.unnamed.b.atv.model.TreeNode;

import java.util.ArrayList;

import velasco.carlos.restclient.ManagementActivity;
import velasco.carlos.restclient.models.SRCollection;

/**
 * Created by Carlos on 03/10/2015.
 */
public abstract class TreeNodeHelper<T> {

    public abstract TreeNode convertItemToTreeNode(Context context, T elem, TreeNode.TreeNodeClickListener itemClickListener);
    public abstract TreeNode convertItemToTreeNode(Context context, T elem, TreeNode.TreeNodeClickListener itemClickListener,boolean editMode);
    public abstract T parseTreeNodeToItem(TreeNode root);
}
