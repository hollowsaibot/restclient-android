package velasco.carlos.restclient.helpers;

import android.content.Context;
import android.util.Log;

import com.unnamed.b.atv.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

import velasco.carlos.restclient.R;
import velasco.carlos.restclient.holders.IconTreeSRPostman;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRFolder;
import velasco.carlos.restclient.models.SRPostman;
import velasco.carlos.restclient.models.SRRequest;

/**
 * Created by Carlos on 03/10/2015.
 */
public class TreeNodeCollectionHelper extends TreeNodeHelper<ArrayList<SRCollection>> {

    @Override
    public TreeNode convertItemToTreeNode(Context context,
                                          ArrayList<SRCollection> collections,
                                          TreeNode.TreeNodeClickListener itemClickListener,
                                          boolean editMode) {

        return this.convertItemToTreeNode(context, collections, null, null, itemClickListener, editMode);
    }

    public TreeNode convertItemToTreeNode(Context context, List<SRCollection> collections, TreeNode.TreeNodeClickListener collectionClickListener, TreeNode.TreeNodeClickListener folderClickListener, TreeNode.TreeNodeClickListener requestClickListener, boolean editMode){

        // root node
        TreeNode root = TreeNode.root();

        for(SRCollection collection : collections){

            // collection
            IconTreeSRPostman<SRCollection> itemCollection = new IconTreeSRPostman<SRCollection>(R.string.ic_book, collection, editMode);
            TreeNode cNode = new TreeNode(itemCollection).setViewHolder(new IconTreeSRPostman.IconTreeCollectionHolder(context, folderClickListener));
            if(collectionClickListener != null) {
                cNode.setClickListener(collectionClickListener);
            }

            // create folders
            for(SRFolder folder : collection.getFolders()){

                IconTreeSRPostman<SRFolder> itemFolder = new IconTreeSRPostman<SRFolder>(R.string.ic_folder, folder, editMode);
                TreeNode fNode = new TreeNode(itemFolder).setViewHolder(new IconTreeSRPostman.IconTreeFolderHolder(context));
                if(folderClickListener != null) {
                    fNode.setClickListener(folderClickListener);
                }

                // create folder childs
                ArrayList<SRRequest> fRequests = SRCollection.getFolderRequests(collection, folder);
                for(SRRequest request : fRequests){
                    IconTreeSRPostman<SRRequest> itemRequest = new IconTreeSRPostman<SRRequest>(R.string.ic_drive_file, request, editMode);
                    TreeNode childFolderNode = new TreeNode(itemRequest).setViewHolder(new IconTreeSRPostman.IconTreeRequestHolder(context));
                    if(requestClickListener != null) {
                        childFolderNode.setClickListener(requestClickListener);
                    }
                    fNode.addChild(childFolderNode);
                }
                cNode.addChild(fNode);
            }

            // create childs
            ArrayList<SRRequest> cRequests = SRCollection.getOrphanedRequests(collection);
            for(SRRequest request : cRequests){
                IconTreeSRPostman<SRRequest> itemRequest = new IconTreeSRPostman<SRRequest>(R.string.ic_drive_file, request, editMode);
                TreeNode childNode = new TreeNode(itemRequest).setViewHolder(new IconTreeSRPostman.IconTreeRequestHolder(context));
                if(requestClickListener != null) {
                    childNode.setClickListener(requestClickListener);
                }
                cNode.addChild(childNode);
            }
            // add collection
            root.addChild(cNode);
        }
        return root;
    }

    @Override
    public ArrayList<SRCollection> parseTreeNodeToItem(TreeNode root) {

        ArrayList<SRCollection> tmpCollections = new ArrayList<>();

        for(TreeNode nodeCollection : root.getChildren()){
            // get node data
            Object collectionHolder = nodeCollection.getViewHolder();
            Class<?> collectionClazz = collectionHolder.getClass();

            if(IconTreeSRPostman.IconTreeCollectionHolder.class.isAssignableFrom(collectionClazz)) {
                Log.d("node", "node collection");
                IconTreeSRPostman<SRCollection> value = (IconTreeSRPostman<SRCollection>) nodeCollection.getValue();
                // copy collection without relations
                SRCollection tmpCollection = SRCollection.createNewInstanceFromData(value.original.getModel(), false);
                tmpCollections.add(tmpCollection);

                // iterate collection
                for (TreeNode nodeRequestFolder : nodeCollection.getChildren()) {

                    Object folderHolder = nodeRequestFolder.getViewHolder();
                    Class<?> folderClazz = folderHolder.getClass();

                    if (IconTreeSRPostman.IconTreeFolderHolder.class.isAssignableFrom(folderClazz)) {
                        // Collection Folders Case
                        Log.d("node", "node folder");
                        IconTreeSRPostman<SRFolder> collectionFolderValue = (IconTreeSRPostman<SRFolder>) nodeRequestFolder.getValue();
                        SRFolder folder = SRFolder.createNewInstanceFromData(collectionFolderValue.original.getModel(), false);
                        tmpCollection.addFolder(folder);
                        // add requests to folder
                        for (TreeNode requestNode : nodeRequestFolder.getChildren()) {
                            // should be a request
                            Object requestHolder = requestNode.getViewHolder();
                            Class<?> requestClazz = requestHolder.getClass();
                            if (IconTreeSRPostman.IconTreeRequestHolder.class.isAssignableFrom(requestClazz)) {
                                // Folder Requests Case
                                Log.d("node", "node folder request");

                                IconTreeSRPostman<SRRequest> folderRequestValue = (IconTreeSRPostman<SRRequest>) requestNode.getValue();
                                tmpCollection.addFolderRequest(folder, folderRequestValue.original.getModel());
                            }
                        }

                    } else if (IconTreeSRPostman.IconTreeRequestHolder.class.isAssignableFrom(folderClazz)) {
                        // Collection requests case
                        Log.d("node", "node request");
                        IconTreeSRPostman<SRRequest> collectionRequestValue = (IconTreeSRPostman<SRRequest>) nodeRequestFolder.getValue();
                        tmpCollection.getRequests().add(collectionRequestValue.original.getModel());
                    } else {

                    }
                }
            }
        }

        // return parsed collection
        return tmpCollections;
    }

    @Override
    public TreeNode convertItemToTreeNode(Context context,
                                          ArrayList<SRCollection> elem,
                                          TreeNode.TreeNodeClickListener itemClickListener) {
        return this.convertItemToTreeNode(context, elem, itemClickListener, false);
    }

    public SRPostman findItemOnTreeNode(TreeNode root, SRPostman elem) {
        SRPostman result = null;
        IconTreeSRPostman<SRPostman> item = (IconTreeSRPostman<SRPostman>) root.getValue();
        if(item != null
            && item.id.equals(elem.getId())) {
            // item found
            result = item.original.getModel();
        } else {
            // look in children
            for (TreeNode node : root.getChildren()) {
                findItemOnTreeNode(node, elem);
            }
        }
        return result;
    }

    public boolean updateItemOnTreeNode(TreeNode root, SRPostman elem) {
        boolean result = false;
        IconTreeSRPostman<SRPostman> item = (IconTreeSRPostman<SRPostman>) root.getValue();
        if(item != null
            && item.id.equals(elem.getId())) {
            // item found
            item.updateModel(elem);
            result = true;
        } else {
            // look in children
            for (TreeNode node : root.getChildren()) {
                result = updateItemOnTreeNode(node, elem);
                if(result) break;
            }
        }
        return result;
    }

    // parse treeNode
}
