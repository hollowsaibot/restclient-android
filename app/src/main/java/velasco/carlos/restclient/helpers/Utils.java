package velasco.carlos.restclient.helpers;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by Carlos on 27/09/2015.
 */
public class Utils {
    public static int dpToPx(int dp, Resources resources) {
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
    public int pxToDp(int px, Resources resources) {
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }
}
