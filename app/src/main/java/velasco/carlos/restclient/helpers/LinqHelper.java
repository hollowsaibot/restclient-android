package velasco.carlos.restclient.helpers;

import android.support.annotation.Nullable;

import com.android.internal.util.Predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Carlos on 20/10/2015.
 */
public class LinqHelper {
    public static <T> List<T> select(Collection<T> items,Predicate<T> predicate){
        ArrayList<T> selectedItems=new ArrayList<T>();
        for (  T item : items) {
            if (predicate.apply(item)) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    public static String join(List<String> list, String conjunction)
    {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String item : list)
        {
            if (first)
                first = false;
            else
                sb.append(conjunction);
            sb.append(item);
        }
        return sb.toString();
    }

    public static <T, V> List<V> map(Collection<T> collection, Function<T, V> function)
    {
        List<V> result = new ArrayList<>();
        for (T item : collection)
        {
            result.add(function.apply(item));
        }
        return result;
    }

    public interface Function<F, T>{

        @Nullable
        T apply(@Nullable F input);
    }
}
