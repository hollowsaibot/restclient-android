package velasco.carlos.restclient.helpers;

import android.content.Context;

import com.github.pwittchen.prefser.library.Prefser;
import com.github.pwittchen.prefser.library.TypeToken;

import java.util.ArrayList;
import java.util.List;

import velasco.carlos.restclient.models.SRCollection;

/**
 * Created by Carlos on 15/11/2015.
 */
public class SRCollectionsManager {

    private static String TAG_SR_PREFS = "SR_COLLECTIONS";
    private static SRCollectionsManager singleton = new SRCollectionsManager();

    // ATTR
    private Context context;
    Prefser prefser;

    public static void start(Context context){
        if(context != null) {
            singleton.context = context.getApplicationContext();
            singleton.prefser = new Prefser(singleton.context);
        }
    }

    public static List<SRCollection> getCollections(){
        // get data
        ArrayList<SRCollection> collections = new ArrayList<SRCollection>();
        if(isStarted()) {
            TypeToken<List<SRCollection>> typeToken = new TypeToken<List<SRCollection>>() {};
            List<SRCollection> tmpCollections = singleton.prefser.get(TAG_SR_PREFS, typeToken, new ArrayList<SRCollection>());
            collections.addAll(tmpCollections);
        }
        // return data
        return collections;
    }

    public static void putCollections(List<SRCollection> collections){
        if(isStarted()) {
            singleton.prefser.put(TAG_SR_PREFS, collections);
        }
    }

    public static void addCollection(SRCollection collection){
        if(isStarted()) {
            List<SRCollection> collections = getCollections();
            collections.add(collection);
            putCollections(collections);
        }
    }

    /* PRIVATE METHODS */
    private static boolean isStarted(){
        return singleton.context != null;
    }

    public static boolean existsCollection(String collectionId) {
        boolean exists = false;
        for(SRCollection collection : getCollections()){
            if(collection.getId().equals(collectionId)){
                exists = true;
                break;
            }
        }
        return exists;
    }

    public static SRCollection findCollection(String collectionId) {
        SRCollection tmpCollection = null;
        for(SRCollection collection : getCollections()){
            if(collection.getId().equals(collectionId)){
                tmpCollection = collection;
                break;
            }
        }
        return tmpCollection;
    }

    public static void updateCollection(SRCollection collection) {
        List<SRCollection> collections = getCollections();
        for(SRCollection c : collections){
            if(c.getId().equals(collection.getId())){
                c.update(collection);
            }
        }
        putCollections(collections);
    }
}
