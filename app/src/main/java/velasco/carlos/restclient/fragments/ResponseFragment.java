package velasco.carlos.restclient.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.R;

/**
 * A placeholder fragment containing a simple send message.
 */
public class ResponseFragment extends Fragment {

    private static final String STATE_CURRENT_HEADER = "TAG_HEADERS";
    private static final String STATE_CURRENT_BODY = "TAG_PARAMS";

    @InjectView(R.id.etResponseHeader) EditText etResponseHeader;
    @InjectView(R.id.etResponseBody)EditText etResponseBody;

    public ResponseFragment() {
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_response, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_CURRENT_HEADER, etResponseHeader.getText().toString());
        outState.putString(STATE_CURRENT_BODY, etResponseBody.getText().toString());
    }

    public void updateResponseFragment(String responseHeader, String responseBody) {
        etResponseHeader.setText(responseHeader);
        etResponseBody.setText(responseBody);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_response, container, false);

        ButterKnife.inject(this, rootView);

        if(savedInstanceState != null){
            String tmpHeader = savedInstanceState.getString(STATE_CURRENT_HEADER);
            String tmpBody = savedInstanceState.getString(STATE_CURRENT_BODY);

            etResponseHeader.setText(tmpHeader);
            etResponseBody.setText(tmpBody);
        }

        return rootView;
    }
}