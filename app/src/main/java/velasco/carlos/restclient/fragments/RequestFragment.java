package velasco.carlos.restclient.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import velasco.carlos.restclient.NetworkResponseRequest;
import velasco.carlos.restclient.R;
import velasco.carlos.restclient.adapters.ParamListAdapter;
import velasco.carlos.restclient.dialogs.AddHeaderDialog;
import velasco.carlos.restclient.dialogs.RequestContainerPickerDialog;
import velasco.carlos.restclient.helpers.LinqHelper;
import velasco.carlos.restclient.helpers.SRCollectionsManager;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.models.SRParam;
import velasco.carlos.restclient.models.SRRequest;
import velasco.carlos.restclient.services.VolleyService;

/**
 * A placeholder fragment containing a simple send message.
 */
public class RequestFragment extends Fragment {

    //private static final String STATE_CURRENT_HEADERS = "TAG_HEADERS";
    //private static final String STATE_CURRENT_PARAMS = "TAG_PARAMS";
    //private static final String STATE_CURRENT_URL = "TAG_URL";
    private static final String STATE_CURRENT_REQUEST = "TAG_REQUEST";

    public static final int CODE_LOAD_REQUEST = 0;
    public static final int CODE_UPDATE_COLLECTION = 1;

    public interface RestCallbackInterface {
        void notifyResponse(String header, String response);
        void collectionUpdated();
    }

    private RequestFragment self = this;
    private Activity context;
    private SRRequest request;
    private Map<String, String> params;
    private Map<String, String> headers;
    private VolleyService vs;
    private RestCallbackInterface mListener;

    @InjectView(R.id.btnSendRequest)
    AppCompatButton btnSendRequest;
    @InjectView(R.id.etRequestUrl)
    EditText etUrl;
    @InjectView(R.id.spinnerRequestMethod)
    Spinner spinner;
    @InjectView(R.id.etRequestParamKey)
    EditText etRequestParamKey;
    @InjectView(R.id.etRequestParamValue)
    EditText etRequestParamValue;
    @InjectView(R.id.btnAddParam)
    AppCompatButton btnAddParam;

    @InjectView(R.id.buttonShowAddParamDialog)
    FloatingActionButton btnNewHeaderParam;
    @InjectView(R.id.requestHeadersListView)
    ExpandableHeightListView lvHeaders;
    @InjectView(R.id.requestParamsListView)
    ExpandableHeightListView lvParams;
    private ParamListAdapter adapterHeaders;
    private ParamListAdapter adapterParams;

    public RequestFragment() {
        this.vs = new VolleyService();
        this.request = new SRRequest();
        this.headers = new HashMap<String, String>();
        this.params = new HashMap<String, String>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        context.getMenuInflater().inflate(R.menu.menu_request, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_save:
                updateModelFromView(this.request);
                if(this.request.getCollectionId() != null) {
                    SRCollection collection = SRCollectionsManager.findCollection(this.request.getCollectionId());
                    if(collection != null) {
                        collection.updateRequest(request);
                    }
                    SRCollectionsManager.updateCollection(collection);
                    // update menu
                    mListener.collectionUpdated();

                    Toast.makeText(context, getString(R.string.request_updated_message), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, getString(R.string.error_request_not_exists_message), Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_add_to_collection:
                updateModelFromView(this.request);
                openFolderPickerDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateModelFromView(request);
        outState.putSerializable(STATE_CURRENT_REQUEST, request);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
        mListener = (RestCallbackInterface) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_request, container, false);

        setHasOptionsMenu(true);

        ButterKnife.inject(this, rootView);

        lvHeaders.setExpanded(true);
        lvParams.setExpanded(true);

        // restore data
        if(savedInstanceState != null){
            this.request = (SRRequest) savedInstanceState.getSerializable(STATE_CURRENT_REQUEST);
            updateViewFromModel(request);
        }

        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(context);

                int method = vs.getMethods().get(spinner.getSelectedItem().toString());
                String url = etUrl.getText().toString();

                // Request a string response from the provided URL.
                NetworkResponseRequest stringRequest = vs.sendNetworkResponseRequest(method, url,
                        headers,
                        params,
                        new VolleyService.StringServiceCallback() {
                            @Override
                            public void onSuccess(String response) {
                                //saveRequestData("", response);
                                if(mListener != null) {
                                    mListener.notifyResponse("", response);
                                }
                            }

                            @Override
                            public void onSuccess(String response, NetworkResponse network) {
                                //saveRequestData(network.headers.toString(), response);
                                if(mListener != null) {
                                    mListener.notifyResponse(network.headers.toString(), response);
                                }
                            }

                            @Override
                            public void onError(String message) {
                                Toast.makeText(context, context.getString(R.string.REQUEST_ERROR_MSG), Toast.LENGTH_LONG).show();
                            }

                        });
                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        });

        adapterHeaders = new ParamListAdapter(context, this, headers, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvHeaders.invalidateViews();
            }
        });
        lvHeaders.setAdapter(adapterHeaders);

        final Dialog.OnClickListener callback = new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                lvHeaders.invalidateViews();
            }
        };
        // add btnNewHeaderParam listener
        btnNewHeaderParam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // show dialog
                AddHeaderDialog headerDialog = new AddHeaderDialog();
                headerDialog.openModalHeader(context, adapterHeaders, callback);
            }
        });

        adapterParams = new ParamListAdapter(context, this, params, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lvParams.invalidateViews();
                    }
                });
            }
        });
        lvParams.setAdapter(adapterParams);

        btnAddParam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String key = etRequestParamKey.getText().toString();
                String value = etRequestParamValue.getText().toString();
                if (key != null && key.length() > 0 && value != null && value.length() > 0) {
                    //params.put(key, value);
                    params.put(key, value);
                    adapterParams.update();
                    adapterParams.notifyDataSetChanged();
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lvParams.invalidateViews();
                        }
                    });

                    etRequestParamKey.setText("");
                    etRequestParamValue.setText("");
                }
            }
        });

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lvParams.invalidateViews();
                lvHeaders.invalidateViews();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CODE_LOAD_REQUEST){
                SRRequest request = (SRRequest) data.getExtras().getSerializable(SRRequest.class.getName());
                this.request = request;
                updateViewFromModel(request);
            }
            else if(requestCode == CODE_UPDATE_COLLECTION){
                if(mListener != null) {
                    mListener.collectionUpdated();
                }
            }
        }
    }

    private void openFolderPickerDialog(){
        RequestContainerPickerDialog dialog = RequestContainerPickerDialog.newInstance(this.request);
        dialog.setTargetFragment(self, CODE_UPDATE_COLLECTION);
        dialog.show(getActivity().getSupportFragmentManager(), RequestContainerPickerDialog.class.getName());
    }

    private void updateViewFromModel(SRRequest request){
        etUrl.setText(request.getUrl());
        // select method
        List<String> methods = Arrays.asList(context.getResources().getStringArray(R.array.http_methods_array));
        int position = methods.indexOf(request.getMethod());
        spinner.setSelection(position);
        // fill header config
        headers.clear();
        adapterHeaders.update();
        // add try catch
        String requestHeaders = request.getHeaders();
        if (!TextUtils.isEmpty(requestHeaders) && requestHeaders.length() > 0) {
            String[] splitedHeaders = requestHeaders.split("\n");
            for (String tmpHeader : splitedHeaders) {
                String[] splitedHeader = tmpHeader.split(":");
                String key = splitedHeader[0];
                String value = splitedHeader[1];
                headers.put(key, value);
            }
        }
        adapterHeaders.update();
        // fill params
        params.clear();
        adapterParams.update();
        if(!TextUtils.isEmpty(request.getDataMode())) {
            switch (request.getDataMode()) {
                case "params":
                    // TODO: only text type param
                    for (SRParam param : request.getData()) {
                        params.put(param.getKey(), param.getValue());
                    }
                    break;
            }
            adapterParams.update();
        }
        // update list views
        lvHeaders.invalidateViews();
        lvParams.invalidateViews();
    }

    private void updateModelFromView(SRRequest request){

        request.setUrl(etUrl.getText().toString());

        // select method
        String method = spinner.getSelectedItem().toString();
        request.setMethod(method);

        // fill header config
        List<String> requestHeaders = LinqHelper.map(headers.entrySet(), new LinqHelper.Function<Map.Entry<String, String>, String>()
        {
            @Nullable
            @Override
            public String apply(@Nullable Map.Entry<String, String> input) {
                return String.format("%s:%s", input.getKey(), input.getValue());
            }
        });
        String requestHeadersStr = LinqHelper.join(requestHeaders, "\n");
        request.setHeaders(requestHeadersStr);

        // fill params
        request.setDataMode("params");

        List<SRParam> tmpParams = LinqHelper.map(params.entrySet(), new LinqHelper.Function<Map.Entry<String, String>, SRParam>() {
            @Nullable
            @Override
            public SRParam apply(@Nullable Map.Entry<String, String> input) {
                SRParam tmpParam = new SRParam();
                tmpParam.setEnabled(true);
                tmpParam.setKey(input.getKey());
                tmpParam.setValue(input.getValue());
                tmpParam.setType("text");
                return tmpParam;
            }
        });

        request.setData(tmpParams);
    }
}
