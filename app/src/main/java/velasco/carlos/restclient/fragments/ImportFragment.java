package velasco.carlos.restclient.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.URI;
import java.net.URISyntaxException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import velasco.carlos.restclient.R;
import velasco.carlos.restclient.helpers.SRCollectionsManager;
import velasco.carlos.restclient.helpers.SoftKeyboardHelper;
import velasco.carlos.restclient.models.SRCollection;
import velasco.carlos.restclient.services.SRService;

/**
 * A placeholder fragment containing a simple view.
 */
public class ImportFragment extends Fragment {

    @InjectView(R.id.btnImportPostman)
    Button btnImportPostman;

    @InjectView(R.id.etPostmanCollection)
    EditText etPostmanCollection;

    @InjectView(R.id.snackbarPosition)
    View coordinatorLayoutView;
    private ProgressDialog progress;

    public ImportFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_import_manager, container, false);

        ButterKnife.inject(this, rootView);
        progress = new ProgressDialog(getActivity());
        progress.setCancelable(false);
        progress.setMessage(getString(R.string.working));

        coordinatorLayoutView = rootView.findViewById(R.id.snackbarPosition);

        final View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        };

        btnImportPostman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = etPostmanCollection.getText().toString();
                URI uri = null;
                try {

                    // show progress dialog
                    progress.show();

                    // hide keyboard
                    SoftKeyboardHelper.hideKeyboard(getActivity());

                    uri = new URI(path);

                    String[] segments = uri.getPath().split("/");
                    String collectionId = segments[segments.length - 1];
                    Call<SRCollection> call = new SRService().getPostmanService().getCollection(collectionId);
                    call.enqueue(new Callback<SRCollection>() {
                        @Override
                        public void onResponse(final Response<SRCollection> response, Retrofit retrofit) {

                            // hide progress
                            progress.dismiss();

                            if(response.body() != null) {
                                // check if override
                                final SRCollection collection = new SRCollection();
                                collection.update(response.body());

                                if(SRCollectionsManager.existsCollection(collection.getId())){
                                    Runnable replaceCollection = new Runnable() {
                                        @Override
                                        public void run() {
                                            SRCollectionsManager.updateCollection(collection);
                                            getActivity().setResult(Activity.RESULT_OK);

                                            Snackbar.make(coordinatorLayoutView, R.string.snackbar_text, Snackbar.LENGTH_LONG)
                                                    .setAction(R.string.snackbar_action_done, clickListener)
                                                    .show();
                                        }
                                    };
                                    showConfirmDialog(replaceCollection);
                                } else {
                                    SRCollectionsManager.addCollection(collection);
                                    getActivity().setResult(Activity.RESULT_OK);

                                    Snackbar.make(coordinatorLayoutView, R.string.snackbar_text, Snackbar.LENGTH_LONG)
                                            .setAction(R.string.snackbar_action_done, clickListener)
                                            .show();
                                }

                            } else {
                                String errorMessage = getString(R.string.error_retrieving_data);
                                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {

                            // hide progress
                            progress.dismiss();

                            String errorMessage = getString(R.string.error_retrieving_data);
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                            t.printStackTrace();
                        }
                    });
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }

    private void showConfirmDialog(final Runnable confirm) {
        //Ask the user if they want to delete
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.collection_already_exists)
                .setMessage(R.string.really_override)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // delete node
                        confirm.run();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }
}
