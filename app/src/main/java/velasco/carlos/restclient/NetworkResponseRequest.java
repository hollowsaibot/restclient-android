package velasco.carlos.restclient;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class NetworkResponseRequest extends Request<NetworkResponse> {
private final Response.Listener<NetworkResponse> mListener;
private final Map<String, String> params;
private final Map<String, String> headers;

    public NetworkResponseRequest(int method, String url, Response.Listener<NetworkResponse> listener,
                                  Response.ErrorListener errorListener) {
        this(method, url, null, null, listener, errorListener);
    }

    public NetworkResponseRequest(int method, String url, Map<String, String> headers, Map<String, String> params,
                              Response.Listener<NetworkResponse> listener,
                     Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.params = params;
        this.headers = headers;

        mListener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if(headers != null) return headers;
        return super.getHeaders();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if(params != null) return params;
        return super.getParams();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return super.getBody();
    }

    public NetworkResponseRequest(String url, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener) {
    this(Method.GET, url, listener, errorListener);
}

@Override
protected void deliverResponse(NetworkResponse response) {
    mListener.onResponse(response);
}

@Override
protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
    return Response.success(response, HttpHeaderParser.parseCacheHeaders(response));
}

public static String parseToString(NetworkResponse response) {
    String parsed;
    try {
        parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
    } catch (UnsupportedEncodingException e) {
        parsed = new String(response.data);
    }
    return parsed;
}
}